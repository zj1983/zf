# 文件服务简介
>在集群部署Z平台的情况下，为了实现所有集群节点，都调用统一文件存储服务需求，特别开发了独立的文件存储服务程序【zf项目】。

# 帮助文档
>https://blog.csdn.net/qq_38056435/article/details/103117399

# 安装包下载
>https://www.zframeworks.com/#download

# 运行环境要求
>Tomcat9 64位

>JDK1.8 64位

