package com.futvan.z.framework.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class StringUtil {
	public static void main(String[] args) {
		System.out.println(CreatePassword("123"));
	}
	
	/**
	 * 异常信息转字符串
	 * @param e 异常对象
	 * @return String
	 */
	public static String ExceptionToString(Exception e) {
		String result = "";
		if(e!=null && e instanceof Exception) {
			StringWriter sw = new StringWriter();  
			e.printStackTrace(new PrintWriter(sw, true));
			result = sw.toString();
		}
		return result;
	}
	
	/**
	 * 加密方法(可逆加密)
	 * 
	 * @param str
	 * @return
	 */
	public static String jia(String str) {
		String returnvalue = "";
		char[] s1 = str.toCharArray();
		String sb1 = "";
		for (int i = s1.length - 1; i >= 0; i--) {
			int srtchar = s1[i] + 3;// 加3
			sb1 = sb1 + (char) srtchar;
		}
		returnvalue = sb1;
		return returnvalue;
	}

	/**
	 * 解密方法
	 * 
	 * @param str
	 * @return
	 */
	public static String jie(String str) {
		String returnvalue = "";
		char[] s1 = str.toCharArray();
		String sb1 = "";
		for (int i = s1.length - 1; i >= 0; i--) {
			int srtchar = s1[i] - 3;// 减3
			sb1 = sb1 + (char) srtchar;
		}
		returnvalue = sb1;
		return returnvalue;
	}
	
	
	/**
	 * 判读是否为空
	 * @param obj
	 * @return
	 */
	public static String toString(Object obj) {
		String str = String.valueOf(obj);
		if("".equals(str) || str==null || "null".equals(str)) {
			return "";
		}else {
			return str;
		}
	}

	
	private static String getListToString(List<String> list) {
		String reslut = "";
		for (String str : list) {
			reslut = reslut+str;
		}
		return reslut;
	}

	/**
	 * 字符串模糊处理
	 * 
	 * @param txt
	 * @return
	 */
	public static String blurred(String txt) {
		StringBuffer returnvalue = new StringBuffer();
		if (!"".equals(txt) && txt != null) {
			// 默认将字符1/3中间位置字符串变为*号
			for (int i = 0; i < txt.length(); i++) {
				if (i < txt.length() / 3 || i > txt.length() / 3 * 2) {
					returnvalue.append(txt.substring(i, i + 1));
				} else {
					returnvalue.append("*");
				}
			}
		}
		return returnvalue.toString();
	}

	/**
	 * 首字母变大写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstLetterToUpper(String str) {
		String returnvalue = null;
		char[] array = str.toCharArray();
		if (Character.isUpperCase(array[0])) {
			returnvalue = String.valueOf(array);
		} else {
			array[0] -= 32;
			returnvalue = String.valueOf(array);
		}
		return returnvalue;
	}

	/**
	 * 首字母变小写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstLetterToLower(String str) {
		String returnvalue = null;
		char[] array = str.toCharArray();
		if (Character.isLowerCase(array[0])) {
			returnvalue = String.valueOf(array);
		} else {
			array[0] += 32;
			returnvalue = String.valueOf(array);
		}
		return returnvalue;
	}

	/**
	 * 列转行
	 * 
	 * @param list
	 *            数据集合
	 * @param delimiter
	 *            分隔符 例：","
	 * @param bracketsLeft
	 *            左括号符号
	 * @param bracketsRight
	 *            右括号符号
	 * @return String
	 */
	public static String ListToString(List<String> list, String delimiter,
			String bracketsLeft, String bracketsRight) {
		String returnvalue = "";
		for (String string : list) {
			if (bracketsLeft != null && bracketsRight != null) {
				returnvalue += bracketsLeft + string + bracketsRight;
			} else {
				returnvalue += string;
			}
			returnvalue += delimiter;
		}
		if ("".equals(returnvalue) || returnvalue == null) {
			return returnvalue;
		} else {
			return returnvalue.substring(0, returnvalue.length() - 1);
		}
	}

	/**
	 * MD5加密类
	 * 
	 * @param str
	 *            要加密的字符串
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public static String toMD5(String str) {
		StringBuffer buf = new StringBuffer("");
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] byteDigest = md.digest();
			int i;
			for (int offset = 0; offset < byteDigest.length; offset++) {
				i = byteDigest[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		// 32位加密
		return buf.toString();
	}

	/**
	 * 获取汉字串拼音首字母，英文字符不变
	 * 
	 * @param chinese
	 *            汉字串
	 * @return 汉语拼音首字母
	 */
	public static String getFirstSpell(String chinese) {
		StringBuffer pybf = new StringBuffer();
		char[] arr = chinese.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 128) {
				try {
					String[] temp = PinyinHelper.toHanyuPinyinStringArray(
							arr[i], defaultFormat);
					if (temp != null) {
						pybf.append(temp[0].charAt(0));
					}
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}
			} else {
				pybf.append(arr[i]);
			}
		}
		return pybf.toString().replaceAll("\\W", "").trim();
	}
	
	/**
	 * 加密方法(不可逆加密)
	 * 
	 * @param str
	 *            标示信息
	 * @return License码
	 * @throws Exception
	 */
	public static String CreatePassword(String str) {
		String infoMd5 = toMD5(str);
		StringBuffer LicenseInfo = new StringBuffer();
		LicenseInfo.append(toMD5(infoMd5.substring(0, 8)));
		LicenseInfo.append(toMD5(infoMd5.substring(8, 16)));
		LicenseInfo.append(toMD5(infoMd5.substring(16, 24)));
		LicenseInfo.append(toMD5(infoMd5.substring(24, 32)));
		return toMD5(LicenseInfo.toString());
	}
}
