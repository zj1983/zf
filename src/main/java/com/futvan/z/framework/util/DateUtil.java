package com.futvan.z.framework.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	/**
	 * 获取日期
	 */
	public static String getDate() {
		return getDateTime("yyyy-MM-dd");
	}
	
	/**
	 * 获取日期时间
	 */
	public static String getDateTime() {
		return getDateTime("yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 获取日期
	 * @param pattern yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getDateTime(String pattern) {
		SimpleDateFormat dateformat1 = new SimpleDateFormat(pattern);
		return dateformat1.format(new java.util.Date());
	}
	
	/**
	 * 日期格式化
	 * 
	 * @param date
	 * @param pattern
	 *            yyyy-MM-dd yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String FormatDate(Object date, String pattern) {
		if(date!=null) {
			SimpleDateFormat dateformat1 = new SimpleDateFormat(pattern);
			return dateformat1.format(date);
		}else {
			return "";
		}
		
	}
	
}
