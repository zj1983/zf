package com.futvan.z.framework.util;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
/**
 * Json工具类
 * @author zz
 * @CreateDate 2018-09-27
 */
public class JsonUtil {
	/**
	 * JSON 转 POJO
	 */
	public static <T> T getObject(String pojo, Class<T> tclass) {
		try {
			return JSONObject.parseObject(pojo, tclass);
		} catch (Exception e) {
			LogUtil.Error(tclass + "转 JSON 失败");
		}
		return null;
	}

	/**
	 * POJO 转 JSON    
	 */
	public static <T> String getJson(T tResponse){
		String pojo = JSONObject.toJSONString(tResponse);
		return pojo;
	}

	/**
	 * List<T> 转 json 保存到数据库
	 */
	public static <T> String listToJson(List<T> ts) {
		String jsons = JSON.toJSONString(ts);
		return jsons;
	}

	/**
	 * json 转 List<T>
	 */
	public static <T> List<T> jsonToList(String jsonString, Class<T> clazz) {
		if(!"".equals(jsonString) && jsonString!=null) {
			@SuppressWarnings("unchecked")
			List<T> ts = (List<T>) JSONArray.parseArray(jsonString, clazz);
			return ts;
		}else {
			return new ArrayList<T>();
		}
	}


}
