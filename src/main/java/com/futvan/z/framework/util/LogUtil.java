package com.futvan.z.framework.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;


/**
 * 日志工具类
 * @author zz
 * @CreateDate 2018-06-20
 */
public class LogUtil {
	private static Logger logger = Logger.getLogger(SystemUtil.class);
	
	public static void Warn(Object log) {
		logger.warn(log);
	}
	public static void Error(Object log) {
		logger.error(log);
	}
	public static void Error(Object log,Exception e) {
		logger.error(log);
		StringWriter errorsWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(errorsWriter));
		logger.error(errorsWriter);
	}
}
