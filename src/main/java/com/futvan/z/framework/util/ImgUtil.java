package com.futvan.z.framework.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;

import net.coobird.thumbnailator.Thumbnails;
import sun.misc.BASE64Decoder;

public class ImgUtil {
	
	/**
	 * 	压缩图片
	 * @param img_path
	 * @return
	 */
	public static String ImgZip(String img_path) {
		String new_img_path = "";
		if(isImg(img_path)) {
			try {
				//其中的scale是可以指定图片的大小，值在0到1之间，1f就是原图大小，0.5就是原图的一半大小，这里的大小是指图片的长宽。
				//而outputQuality是图片的质量，值也是在0到1，越接近于1质量越好，越接近于0质量越差。
				String temp_img_path_50 = img_path.substring(0, img_path.lastIndexOf(".")) +"_z5." + img_path.substring(img_path.lastIndexOf(".")+1);
				Thumbnails.of(img_path).scale(0.5f).outputQuality(0.5f).toFile(temp_img_path_50);
				
				String temp_img_path_20 = img_path.substring(0, img_path.lastIndexOf(".")) +"_z1." + img_path.substring(img_path.lastIndexOf(".")+1);
				Thumbnails.of(img_path).scale(0.1f).outputQuality(0.5f).toFile(temp_img_path_20);
				
				new_img_path = temp_img_path_50;
			} catch (IOException e) {
				LogUtil.Error("压缩图片出错|原图片地址："+img_path, e);
			}
		}
		return new_img_path;
		
	}
	
	/**
	 * 	判读文件名称是否是图片类型
	 * @param filename
	 * @return
	 */
	public static boolean isImg(String filename) {
		boolean result = false;
		if(filename!=null && !"".equals(filename) && !"null".equals(filename) && !"NULL".equals(filename) && !"Null".equals(filename) && !"NuLL".equals(filename)) {
			if(filename.indexOf(".jpg")>=0){
				result = true;
			}else if(filename.indexOf(".png")>=0) {
				result = true;
			}else if(filename.indexOf(".jpeg")>=0) {
				result = true;
			}else if(filename.indexOf(".bmp")>=0) {
				result = true;
			}else if(filename.indexOf(".gif")>=0) {
				result = true;
			}else if(filename.indexOf(".tiff")>=0) {
				result = true;
			}else if(filename.indexOf(".raw")>=0) {
				result = true;
			}
		}
		return result;
	}
	
	public static Result Base64ToImg(String path,String imgdate) {
		Result result = new Result();
		if(imgdate!=null && !"".equals(imgdate) && !"null".equals(imgdate) && !"NULL".equals(imgdate) && !"Null".equals(imgdate) && !"NuLL".equals(imgdate)) {
			BASE64Decoder decoder = new BASE64Decoder();
			//去掉base64,头描述信息
			if(imgdate.indexOf(";base64,")>=0) {
				imgdate = imgdate.substring(imgdate.indexOf(";base64,")+8);
			}
	        try {
	            // Base64解码
	            byte[] bytes = decoder.decodeBuffer(imgdate);
	            for (int i = 0; i < bytes.length; ++i) {
	                if (bytes[i] < 0) {// 调整异常数据
	                    bytes[i] += 256;
	                }
	            }
	            // 生成jpeg图片
	            OutputStream out = new FileOutputStream(path);
	            out.write(bytes);
	            out.flush();
	            out.close();
	            result.setCode(Code.SUCCESS);
	            result.setMsg("转换成功");
	        } catch (Exception e) {
	        	result.setCode(Code.ERROR);
	        	result.setMsg(e.getMessage());
	        }
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("imgdate is null");
		}
		return result;
	}

}
