package com.futvan.z.framework.util;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.futvan.z.framework.common.bean.Code;

/**
 * Z System Util
 * @author zz
 * @CreateDate 2018-06-08
 */
public class SystemUtil {

	public static void main(String[] args) throws Exception {
	}
	
	/**
	 * 	创建ZID
	 * @param tableId 表ID
	 * @return
	 */
	public static String newZid() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * 创建编号
	 * @param TableName 表名
	 * @return
	 */
	public static String newNumber(){
		return newNumber("","yyyyMMddHHmmssSSS",6);
	}

	/**
	 * 创建编号
	 * @param head 标识
	 * @param pattern 时间格式
	 * @param digit 随机长度
	 * @return
	 */
	public static String newNumber(String head,String pattern,int digit){
		StringBuffer returnvalue = new StringBuffer();

		//生成头标示
		if(!"".equals(head) && head!=null){
			returnvalue.append(head);
		}
		//生成日期标示
		String dateInfo = DateUtil.FormatDate(new java.util.Date(),pattern);
		returnvalue.append(dateInfo);

		//生成随机数标示
		Random random = new Random();
		String fd = "";
		for (int i = 0; i < digit; i++) {
			fd = fd + "0";
		}
		returnvalue.append(MathUtil.FormatNumber((random.nextInt(new BigDecimal("1"+fd).intValue())%(new BigDecimal("1"+fd).intValue()-1+1) + 1),fd));

		return returnvalue.toString();
	}


	/** 
	 * Map转换层Bean，使用泛型免去了类型转换的麻烦。 
	 * @param <T> 
	 * @param map   
	 * @param class1 
	 * @return 
	 */  
	public static <T> T MapToBean(Map<String, String> map, Class<T> class1) {  
		T bean = null;  
		try {  
			bean = class1.newInstance();  
			BeanUtils.populate(bean, map);  
		} catch (Exception e) {  
			e.printStackTrace();  
		} 
		return bean;  
	}
	
}
