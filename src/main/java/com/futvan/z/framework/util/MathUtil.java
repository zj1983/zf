package com.futvan.z.framework.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MathUtil {
	/**
	 * 数字格式化
	 * @param Number int double long
	 * @param pattern 0000  000.00
	 * @return
	 */
	public static String FormatNumber(Object Number,String pattern){
		String relust = "";
		try {
			BigDecimal n = new BigDecimal(String.valueOf(Number));
			DecimalFormat df = new DecimalFormat(pattern);
			//向零方向舍入的舍入模式。从不对舍弃部分前面的数字加 1（即截尾）。
			//注意，此舍入模式始终不会增加计算值的绝对值。
			df.setRoundingMode(RoundingMode.DOWN);
			relust = df.format(n);
		} catch (Exception e) {
			LogUtil.Warn("数字格式化出错："+Number+"   条件："+pattern);
		}
		return relust;
	}
	
	/**
	 * 根据数据总数与每页显示数，计算总页数
	 * @param datacount
	 * @param rowcount
	 * @return
	 */
	public static String getPageCount(int datacount,int rowcount) {
		int pagecount = 0;
		if(datacount>0 && rowcount>0) {
			BigDecimal datacount_big = new BigDecimal(datacount);
			pagecount = datacount_big.divide(new BigDecimal(rowcount), BigDecimal.ROUND_UP).intValue();
		}
		return String.valueOf(pagecount);
	}
	
	/**
	 * 获取两个数这间的随机数
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandom(int min,int max) {
		int i = (int)(min+Math.random()*(max-min+1));
		return i;
	}
}
