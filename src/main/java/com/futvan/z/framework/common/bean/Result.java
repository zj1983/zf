package com.futvan.z.framework.common.bean;

/**
 * 返回对象
 * @author zz
 * @param <T>
 * @CreateDate 2018-06-08
 */
public class Result<T> {
	private Code code;//状态码
	private String msg;//信息
	private T data;//数据
	public Code getCode() {
		return code;
	}
	public void setCode(Code code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
