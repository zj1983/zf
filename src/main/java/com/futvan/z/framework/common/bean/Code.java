/**
 * 
 */
package com.futvan.z.framework.common.bean;

/**
 * @author zz
 * @CreateDate 2018-07-05
 */
public enum Code {
	SUCCESS("success"),//成功
	ERROR("error");//出错
	private String ncode;
	private Code(String ncode) {
		this.ncode = ncode;
	}
}
