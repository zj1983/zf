package com.futvan.z.framework.core;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.jsp.tagext.BodyTagSupport;
import org.springframework.beans.factory.InitializingBean;

import com.futvan.z.framework.util.StringUtil;

/**
 * Z 超类
 * @author zz
 * @CreateDate 2018-08-21
 */
public class SuperZ extends BodyTagSupport implements InitializingBean{
	public static Map<String,String> sp;//系统参数
	
	/**
	 * 初始化系统参数
	 * @throws Exception 
	 */
	private void initSP() throws Exception {
		sp = new HashMap<String, String>();

		//获取z.properties中的参数
		Properties prop = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("z.properties");
		prop.load(in);
		for (String key : prop.stringPropertyNames()) {  
			sp.put(key, prop.getProperty(key).trim());
		}

	}

	public void afterPropertiesSet() throws Exception {
		initSP();
	}
	
	/**
	 * 是否不为空
	 * @return
	 */
	public static boolean isNotNull(Object obj) {
		if(obj!=null && !"".equals(obj) && !"null".equals(obj) && !"NULL".equals(obj) && !"Null".equals(obj) && !"NuLL".equals(obj)) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * 是否为空
	 * @return
	 */
	public static boolean isNull(Object obj) {
		if(obj==null || "".equals(obj) || "null".equals(obj) || "NULL".equals(obj) || "Null".equals(obj) || "NuLL".equals(obj)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * URL特殊字符转义还原
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	protected String UrlDecode(String str) throws UnsupportedEncodingException {
		return URLDecoder.decode(str, "utf-8");
	}
	
	/**
	 * 	创建ZID
	 * @param tableId 表ID
	 * @return
	 */
	public static String newZid() {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		return uuid;
	}
	
}
