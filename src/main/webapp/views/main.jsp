<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common.jsp"%>
<script type="text/javascript">

/**
 * 打开文件上传页面
 * @param TableColunmId
 * @returns
 */
function uploadFile(){
	var width = 400;
	var height = 250;
	var iTop = (window.screen.availHeight-30-height)/2;//获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth-10-width)/2;//获得窗口的水平位置;
	window.open('openUploadFile', '文件上传', 'width='+width+',height='+height+',location=no,menubar=no,status=no,toolbar=no, top='+iTop+', left='+iLeft);
}

/**
 * 刷新列表
 */
function reloadList(){
	window.location.reload();
}

/**
 * 退出
 */
function ExitZF(){
	if (confirm('您确定要退出文件管理平台吗？')==true){ 
		window.location.href='index';
	} 
}

/**
 * 删除文件
 */
function deleteFile(zid,name){
	if (confirm('您确定删除 '+name+' 文件吗？')==true){ 
		$.ajax({
			type : "get",
			url : 'deleteFile',
			data:{zid:zid},
			success : function(data) {
				if(data.code=='SUCCESS'){
					reloadList();
				}else{
					alert(''+data.msg);
				}
			},
			error: function (data) {
				alert('ajax错误：'+JSON.stringify(data));
			}
		});
	}
}


</script>
</head>
<body>
	<!-- 按钮区域 -->
	<div class="container-fluid mt-3 mb-3">
		<div class="row">
			<div class="col-6">
				<button type="button" class="btn btn-outline-info btn-lg" onclick="reloadList()">刷新列表</button> 
				<button type="button" class="btn btn-outline-info btn-lg" onclick="uploadFile()">上传新文件</button>
			</div>
			<div class="col-6 d-flex justify-content-end">
				<button type="button" class="btn btn-outline-danger btn-lg" onclick="ExitZF()">退 出</button> 
			</div>
		</div>
	</div>
	<!-- 文件列表 -->
	<div class="container-fluid">
			<div class="row d-flex align-items-center border-top border-bottom">
				<div class="col-md-1 border-right">
				</div>
				<div class="col-md-2 border-right">
					创建时间
				</div>
				<div class="col-md-2 border-right">
					文件名称
				</div>
				<div class="col-md-1 border-right">
					文件大小
				</div>
				<div class="col-md-6 border-right">
					访问地址
				</div>
			</div>
		<c:forEach items="${filelist}" var="f">
			<div class="row d-flex align-items-center border-bottom">
				<div class="col-md-1 border-right">
					<button type="button" class="btn btn-danger" onclick="deleteFile('${f.zid}','${f.name}')">删 除</button>
				</div>
				<div class="col-md-2 border-right">
					${f.create_time}
				</div>
				<div class="col-md-2 border-right text-wrap" style="word-break:break-all">
					<a href="${f.url}" target="_blank">${f.name}</a>
				</div>
				<div class="col-md-1 border-right">
					${f.size}
				</div>
				<div class="col-md-6 border-right text-wrap" style="word-break:break-all">
					${f.url}
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>