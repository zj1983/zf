<%@ page pageEncoding="utf-8"%>
<title>ZF文件管理平台</title>
<meta name="referrer" content="no-referrer" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- 系统ICO图标 -->
<link href="<%=request.getContextPath()%>/img/zico.ico" rel="shortcut icon" type="image/x-icon"  media="screen,print" />

<!-- 引用小图标 -->
<link  href="<%=request.getContextPath()%>/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen,print"/>

<!-- 添加jQuery -->
<script src="<%=request.getContextPath()%>/js/jquery-3.3.1.min.js"></script>

<!-- 添加BootStrap -->
<link  href="<%=request.getContextPath()%>/css/bootstrap-4.0.0/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen,print"/>
<script src="<%=request.getContextPath()%>/css/bootstrap-4.0.0/js/bootstrap.min.js"></script>

<!-- 文件上传控件 -->
<link href="<%=request.getContextPath()%>/js/uploadifive/uploadifive.css" type="text/css" rel="stylesheet" media="screen,print">
<script src="<%=request.getContextPath()%>/js/uploadifive/jquery.uploadifive.min.js" type="text/javascript"></script>


<style>
/* 高度等于屏幕高度 */
.c_height{
	height: calc(100vh);
}
</style>

<script type="text/javascript">
/**
 * 判断是否为空
 * @param str
 * @returns {Boolean}
 */
function isNull(str){
	if(str == '' || str == null || typeof(str) == "undefined" || str == "undefined" || str == "null" || str == null){
		return true;
	}else{
		return false;
	}
}
/**
 * 判断是否不为空
 * @param str
 * @returns {Boolean}
 */
function isNotNull(str){
	if(str != '' && str != null && typeof(str) != "undefined" && str != "undefined" && str != "null" && str != null){
		return true;
	}else{
		return false;
	}
}
</script>

