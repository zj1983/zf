<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>文件上传</title>
<%@include file="/views/common.jsp"%>
<script type="text/javascript">
$(function() {
	$("#uploadify").uploadifive({
		uploadScript:'upload_no_save_zip?filepath='+$("#zid").val(),
		auto: true,
		fileDataName:'fileData',
		fileObjName:'fileData',
		buttonText: '请选择要上传的文件',
		queueID: 'queue',
		height: 50,
		width : 220,
		multi: false,
		onFallback:function(){    
            $("#alertInfo").text("您的浏览器不支持HTML5文件上传控件，请使用支持HTML5的浏览器后使用。如：Chrome")
        },onUploadComplete : function(file, data){  
        	var obj = JSON.parse(data);
            if (obj.code == "SUCCESS") {
                window.opener.reloadList();
                window.close();
            } else {
            	 $("#alertInfo").text(data.msg + " | "+obj.data)
            }
        },onAddQueueItem:function(file){
        	if(/.*[\u4e00-\u9fa5]+.*$/.test(file.name)) { 
        		$("#alertInfo").text("文件名,不能包含中文!")
        		$("#uploadify").uploadifive('cancel',$('.uploadifive-queue-item').first().data('file')); 
        		$("#uploadify").uploadifive('clearQueue');
        	}
        }
	});
});
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid" >
		<input type="hidden" id="zid" value="${zid}" />
		<input id="uploadify" name="fileData" type="file"/>
	</div>
	<div class="row-fluid" >
		<span id="alertInfo"></span>
		<div id="queue"></div>
	</div>
</div>
</body>
</html>