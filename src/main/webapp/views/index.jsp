<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common.jsp"%>
</head>
<body>
	<div class="container">
		<div class="row justify-content-center align-self-center c_height">
			<div class="col-4 align-self-center border rounded p-4">
				<form action="UserLogin" method="post" id="LoginForm" enctype="multipart/form-data">
					<h3 class="pb-2 text-center">ZF文件管理平台</h3>
					<div class="form-group">
						<label>账号</label>
						<input type="text" class="form-control" name="username">
					</div>
					<div class="form-group">
						<label>密码</label>
						<input type="password" class="form-control" name="password">
					</div>
					<button type="submit" class="btn btn-outline-primary btn-lg btn-block mt-3 mb-3">登录</button>
					<small class="form-text text-danger mb-3">${info}</small>
				</form>
			</div>
		</div>
	</div>
</body>
</html>